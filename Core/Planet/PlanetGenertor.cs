﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Exceptions;
using AmadeusRTG.Core.Planet.Defenitions;
using AmadeusRTG.Core.Planet.Mesh;
using AmadeusRTG.Core.Threads;
using UnityEngine;

namespace AmadeusRTG.Core.Planet
{
    public class PlanetGenertor
    {
        public GameObject PlanetContainerObject { get; private set; }
        protected int DivisionLevel { get; private set; }
        public double NoiseFactor { get; private set; }
        public PlanetMeshDefenition PlanetMeshDefinitionInfo { get; private set; }
        // list of terrain surfaces
        private List<SurfaceMesh> surfaces = new List<SurfaceMesh>();
        public PlanetGenertor(PlanetMeshDefenition PlanetMeshDefinitionInfo, GameObject PlanetContainerObject, double noiseFactor)
        {
            if (PlanetContainerObject == null)
                throw new PlanetHasNoContainerObjectException();
            if (PlanetMeshDefinitionInfo == null) 
                throw new PlanetMeshDefinitionNullException();
            DivisionLevel = 1;
            this.PlanetMeshDefinitionInfo = PlanetMeshDefinitionInfo;
            this.PlanetContainerObject = PlanetContainerObject;
            NoiseFactor = noiseFactor;
        }

        #region Planet Genertor Methods

        public void Generate(PlanetDefenition PlanetDefinitionInfo)
        {

            if (PlanetDefinitionInfo == null)
                throw new PlanetDefinitionNullException();
            //we start the generate planet proc within a theard
            ThreadMonitor.RunOnThread(() =>
            {
                try
                {
                    PlanetContainerObject.name = GeneretePlanetName();
                    PlanetMesh mesh = new PlanetMesh();
                    mesh.Generate(PlanetDefinitionInfo, PlanetMeshDefinitionInfo, NoiseFactor);
                    GameObject MeshPlanet = mesh.gameObject;
                    MeshPlanet.name = string.Format("Planet_{0}_Mesh_data", PlanetContainerObject.name);
                    MeshPlanet.transform.parent = PlanetContainerObject.transform;
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
                finally
                {
                    PlanetContainerObject.transform.localPosition = PlanetDefinitionInfo.StartPostion;
                    ///@todo here we added up to planet meta data for orbit and rotation and gravity effect
                }
            });
        }

        private string GeneretePlanetName()
        {
            return "PlanetHolder";
        }

        #endregion

    }
}
