﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Planet.Mesh;

namespace AmadeusRTG.Core.Planet
{
    public class SurfacePriorityComparer : IComparer<SurfaceMesh>
    {

        public int Compare(SurfaceMesh x, SurfaceMesh y)
        {
            if (x.priorityPenalty < y.priorityPenalty)
                return -1;
            if (x.priorityPenalty > y.priorityPenalty)
                return 1;
            else
                return 0;
        }
    }
}
