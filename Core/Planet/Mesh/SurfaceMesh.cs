﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using AmadeusRTG.Core.Threads;
using AmadeusRTG.Core.Utils.Enums;
using UnityEngine;

namespace AmadeusRTG.Core.Planet.Mesh
{
    public class SurfaceMesh : MonoBehaviour
    {

        // dimensions of the surface
        public Vector3 topleft, bottomright, topright, bottomleft;

        // mesh
        public UnityEngine.Mesh mesh;

        // reference to planet and parent surface
        public PlanetMesh planet;
        public SurfaceMesh parent;
        public SurfacePosition surfacePosition;
        public int subX, subY;

        // LOD
        public int lodLevel = 0;
        public bool queuedForSubdivision = false;

        public delegate void SurfaceDelegate(SurfaceMesh s);
        public event SurfaceDelegate OnGenerationComplete, OnSubdivisionComplete;

        // modifier
        public float modifierStartX, modifierStartY;
        public float modifierResolution, modifierMultiplier;
        public List<SurfaceMesh> subSurfaces { get; set; }

        // uv
        public float uvStartX, uvStartY, uvResolution;

        // distance and angle from camera
        public float distance = 0f, angle = 0f;

        // closest corner for culling
        public Vector3 closestCorner;
        public Vector3 topLeftCorner, topRightCorner, bottomLeftCorner, bottomRightCorner, middlePoint;

        // list of child surfaces
        public bool hasSubsurfaces = false;
        private int generatedCount = 0;

        // generation priority value
        public float priorityPenalty = 0f;

        /// <summary>
        /// Initializes the surface, called by the genetor to pass parameters
        /// </summary>
        public void Initialize(int level, Vector3 start, Vector3 end, Vector3 topRight, Vector3 bottomLeft,
            PlanetMesh planet, SurfacePosition sp, SurfaceMesh parent, int sx, int sy)
        {
            //Initializes System params 
            lodLevel = level;
            this.planet = planet;
            surfacePosition = sp;
            this.parent = parent;
            this.subX = sx;
            this.subY = sy;

            // surface start and end points
            this.topleft = start;
            this.bottomright = end;
            InitializeCalculateMesh(start,end,topRight,bottomLeft);
        }

        #region Initialize Mesh Methods
        /// <summary>
        /// we initialize calculate and generate mesh dataMesh
        /// </summary>
        public void InitializeCalculateMesh(Vector3 start, Vector3 end, Vector3 topRight, Vector3 bottomLeft)
        {
            // modifier calculate starting point
            modifierStartX = modifierStartY = 0;
            if (lodLevel == 0) //we at first level of the mesh Calculate level
            {
                modifierResolution = planet.PlanetMeshDefinitionInfo.Resolution/
                                     (float) planet.PlanetMeshDefinitionInfo.SubdivisionsLevel;
            }
            else// we check the modifier resolution based our level of the input
            {
                ///@todo lod level based change 
            }
            modifierStartX += subX * modifierResolution;
            modifierStartY += subY * modifierResolution;
            modifierMultiplier = modifierResolution/planet.PlanetMeshDefinitionInfo.Resolution;
            // uv calculate of the mesh
            uvStartX = 0f;
            uvStartY = 0f;

            if (lodLevel == 0)//we at first level of the mesh Calculate level
            {
                uvResolution = 1f / planet.PlanetMeshDefinitionInfo.SubdivisionsLevel;
            }
            else// we check the modifier resolution based our level of the input
            {
                ///@todo lod level based change  (UV)
            }

            // corners
            subSurfaces = new List<SurfaceMesh>();
            if (planet.UseLod)
            {

                topLeftCorner = SperifyPoint(topleft) * planet.PlanetMeshDefinitionInfo.Radius;
                bottomRightCorner = SperifyPoint(bottomright) * planet.PlanetMeshDefinitionInfo.Radius;

                middlePoint = SperifyPoint(((start + end) / 2f)) * planet.PlanetMeshDefinitionInfo.Radius;

                topRightCorner = SperifyPoint(topRight) * planet.PlanetMeshDefinitionInfo.Radius;
                bottomLeftCorner = SperifyPoint(bottomLeft) * planet.PlanetMeshDefinitionInfo.Radius;
            }

            // mesh
            if (mesh == null)
            {
                // create mesh filter
                MeshFilter meshFilter = (MeshFilter)gameObject.AddComponent(typeof(MeshFilter));
                mesh = meshFilter.sharedMesh = new UnityEngine.Mesh();

                // create mesh renderer
                gameObject.AddComponent(typeof(MeshRenderer));
                renderer.castShadows = true;
                renderer.receiveShadows = true;
                renderer.enabled = true;
            }
        }
        /// <summary>
        /// we calculate surface points in the space
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 SperifyPoint(Vector3 point)
        {
            float dX2, dY2, dZ2;
            float dX2Half, dY2Half, dZ2Half;

            dX2 = point.x * point.x;
            dY2 = point.y * point.y;
            dZ2 = point.z * point.z;

            dX2Half = dX2 * 0.5f;
            dY2Half = dY2 * 0.5f;
            dZ2Half = dZ2 * 0.5f;

            point.x = point.x * Mathf.Sqrt(1f - dY2Half - dZ2Half + (dY2 * dZ2) * (1f / 3f));
            point.y = point.y * Mathf.Sqrt(1f - dZ2Half - dX2Half + (dZ2 * dX2) * (1f / 3f));
            point.z = point.z * Mathf.Sqrt(1f - dX2Half - dY2Half + (dX2 * dY2) * (1f / 3f));

            return point;
        }
        #endregion

        #region Genrate Mesh Data 

        /// <summary>
        /// Creates a mesh with the given amount of detail (vertices)
        /// </summary>
        /// <param name="detail">
        /// A <see cref="System.Int32"/>
        /// </param>
        public void GenerateMesh(int detail)
        {
            // mesh data
            Vector3[] vertices = null;
            Color[] vertexColors = null;
            Vector2[] uvs = null;
            Vector3[] normals = null;
            int[] indexbuffer = null;
            Action ApplyMesh = () =>
            {
                if (mesh != null)
                {
                    // create mesh
                    mesh.vertices = vertices;
                    mesh.colors = vertexColors;
                    mesh.triangles = indexbuffer;
                    mesh.uv = uvs;
                    mesh.normals = normals;
                    mesh.RecalculateNormals();
                    mesh.RecalculateBounds();
                }
                if (this != null)
                {
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.position = transform.parent.position;
                    transform.rotation = transform.parent.rotation;
                }
                // set mesh to collider
                if (planet.generateColliders[lodLevel])
                {
                    MeshCollider collider = (MeshCollider) gameObject.GetComponent(typeof (MeshCollider));
                    if (collider == null)
                        collider = (MeshCollider) gameObject.AddComponent(typeof (MeshCollider));
                    collider.sharedMesh = mesh;
                }

                // report to planet
                planet.ReportGeneration(this);

                // event to parent surface
                if (OnGenerationComplete != null)
                    OnGenerationComplete(this);
            };

            if (Application.isPlaying)
            {
                ThreadMonitor.RunOnThread(() => {
                    CalculateGeometry(detail, out vertices, out vertexColors, out uvs, out normals, out indexbuffer);
                    ThreadMonitor.RunOnThread(ApplyMesh);
                });
            }
        }

        private void CalculateGeometry(int detail, out Vector3[] vertices, out Color[] vertexColors, out Vector2[] uvs, out Vector3[] normals, out int[] indexbuffer)
        {
            // measure execution time
            System.DateTime startTime = System.DateTime.UtcNow;
            // downward facing vertices at border of the surface to prevent seams appearing
            int borders = detail * 4;
            // vertex array
            vertices = new Vector3[detail * detail + borders];

            // vertex colours, to store data in
            vertexColors = new Color[detail * detail + borders];
            // calculate interpolation between coordinates
            float stepX = (bottomright.x - topleft.x) / (detail - 1);
            float stepY = (bottomright.y - topleft.y) / (detail - 1);
            float stepZ = (bottomright.z - topleft.z) / (detail - 1);
            // check which axis remains stationary
            bool staticX = false, staticY = false, staticZ = false;
            if (stepX == 0)
                staticX = true;
            if (stepY == 0)
                staticY = true;
            if (stepZ == 0)
                staticZ = true;
            // normals
            normals = new Vector3[detail * detail + borders];
            Vector3 line1 = Vector3.zero;
            Vector3 line2 = Vector3.zero;
            // uvs
            uvs = new Vector2[detail * detail + borders];
            // indices
            indexbuffer = new int[(detail - 1) * (detail - 1) * 6 + (borders - 4) * 6];
            int index = 0;
            // plot mesh geometry
            for (int col = 0; col < detail; col++)
            {
                for (int row = 0; row < detail; row++)
                {
                    // set vertex position
                    if (staticX)
                        vertices[col * detail + row] = new Vector3(topleft.x, topleft.y + stepY * col, topleft.z + stepZ * row);
                    if (staticY)
                        vertices[col * detail + row] = new Vector3(topleft.x + stepX * row, topleft.y, topleft.z + stepZ * col);
                    if (staticZ)
                        vertices[col * detail + row] = new Vector3(topleft.x + stepX * row, topleft.y + stepY * col, topleft.z);

                    // map the point on to the sphere
                    vertices[col * detail + row] = SperifyPoint(vertices[col * detail + row]);


                    // displace with noise values
                    float displacement =  (float)planet.PlanetMeshDefinitionInfo.PresetNoiseer.GetValue(vertices[col * detail + row]);

                    float rx = modifierStartX + row * modifierMultiplier;
                    float cy = modifierStartY + col * modifierMultiplier;
                    if (planet.useBicubicInterpolation)
                        displacement += planet.GetBicubicInterpolatedModifierAt(rx, cy, surfacePosition);
                    else
                        displacement += planet.GetBilinearInterpolatedModifierAt(rx, cy, surfacePosition);

                    // displace vertex position
                    vertices[col * detail + row] += vertices[col * detail + row] * displacement * planet.PlanetMeshDefinitionInfo.HeightVariation;

                    // store data to vertex color:, r = height, g = polar, b = unused
                    vertexColors[col * detail + row] = new Color(displacement, Mathf.Abs(vertices[col * detail + row].y), 0f);

                    // scale to planet radius
                    vertices[col * detail + row] *= planet.PlanetMeshDefinitionInfo.Radius;

                    // calculate uv's
                    uvs[col * detail + row] = new Vector2(uvStartX + ((float)(row + 1) / detail) * uvResolution, uvStartY + ((float)(col + 1) / detail) * uvResolution);

                    // calculate triangle indexes
                    if (col < detail - 1 && row < detail - 1)
                    {
                        indexbuffer[index] = (col * detail + row);
                        index++;
                        indexbuffer[index] = (col + 1) * detail + row;
                        index++;
                        indexbuffer[index] = col * detail + row + 1;
                        index++;

                        indexbuffer[index] = (col + 1) * detail + row;
                        index++;
                        indexbuffer[index] = (col + 1) * detail + row + 1;
                        index++;
                        indexbuffer[index] = col * detail + row + 1;
                        index++;
                    }

                    // CALCULATE NORMALS
                    int previousCol = col - 1;
                    int previousRow = row - 1;
                    if (previousCol >= 0)
                    {
                        line1.x = vertices[col * detail + row].x - vertices[previousCol * detail + row].x;
                        line1.y = vertices[col * detail + row].y - vertices[previousCol * detail + row].y;
                        line1.z = vertices[col * detail + row].z - vertices[previousCol * detail + row].z;
                    }
                    else
                    {
                        Vector3 previous = Vector3.zero;

                        if (staticX)
                            previous = new Vector3(topleft.x, topleft.y - stepY, topleft.z + stepZ * row);
                        if (staticY)
                            previous = new Vector3(topleft.x + stepX * row, topleft.y, topleft.z - stepZ);
                        if (staticZ)
                            previous = new Vector3(topleft.x + stepX * row, topleft.y - stepY, topleft.z);

                        previous = SperifyPoint(previous);
                        float disp = (float)planet.PlanetMeshDefinitionInfo.PresetNoiseer.GetValue(previous);

                        rx = modifierStartX + row * modifierMultiplier;
                        cy = modifierStartY + previousCol * modifierMultiplier;
                        if (planet.useBicubicInterpolation)
                            disp += planet.GetBicubicInterpolatedModifierAt(rx, cy, surfacePosition);
                        else
                            disp += planet.GetBilinearInterpolatedModifierAt(rx, cy, surfacePosition);

                        previous += previous * planet.PlanetMeshDefinitionInfo.HeightVariation * disp;
                        previous *= planet.PlanetMeshDefinitionInfo.Radius;

                        line1.x = vertices[col * detail + row].x - previous.x;
                        line1.y = vertices[col * detail + row].y - previous.y;
                        line1.z = vertices[col * detail + row].z - previous.z;
                    }

                    if (previousRow >= 0)
                    {
                        line2.x = vertices[col * detail + row].x - vertices[col * detail + previousRow].x;
                        line2.y = vertices[col * detail + row].y - vertices[col * detail + previousRow].y;
                        line2.z = vertices[col * detail + row].z - vertices[col * detail + previousRow].z;
                    }
                    else
                    {
                        Vector3 previous = Vector3.zero;

                        if (staticX)
                            previous = new Vector3(topleft.x, topleft.y + stepY * col, topleft.z - stepZ);
                        if (staticY)
                            previous = new Vector3(topleft.x - stepX, topleft.y, topleft.z + stepZ * col);
                        if (staticZ)
                            previous = new Vector3(topleft.x - stepX, topleft.y + stepY * col, topleft.z);

                        previous = SperifyPoint(previous);
                        float disp = (float)planet.PlanetMeshDefinitionInfo.PresetNoiseer.GetValue(previous);

                        rx = modifierStartX + previousRow * modifierMultiplier;
                        cy = modifierStartY + col * modifierMultiplier;
                        if (planet.useBicubicInterpolation)
                            disp += planet.GetBicubicInterpolatedModifierAt(rx, cy, surfacePosition);
                        else
                            disp += planet.GetBilinearInterpolatedModifierAt(rx, cy, surfacePosition);

                        previous += previous * planet.PlanetMeshDefinitionInfo.HeightVariation * disp;
                        previous *= planet.PlanetMeshDefinitionInfo.Radius;

                        line2.x = vertices[col * detail + row].x - previous.x;
                        line2.y = vertices[col * detail + row].y - previous.y;
                        line2.z = vertices[col * detail + row].z - previous.z;
                    }

                    normals[col * detail + row] = Vector3.Cross(line1, line2);
                    normals[col * detail + row].Normalize();
                }
            }

            // borders
            float borderDepth = -0.025f;
            int vertexCount = detail * detail;
            for (int col = 0; col < detail; col++)
            {
                int row = 0;
                vertices[vertexCount] = vertices[col * detail + row] + vertices[col * detail + row].normalized * planet.PlanetMeshDefinitionInfo.Radius * borderDepth;
                normals[vertexCount] = normals[col * detail + row];
                uvs[vertexCount] = uvs[col * detail + row];
                vertexColors[vertexCount] = vertexColors[col * detail + row];

                // calculate triangle indexes
                if (col < detail - 1)
                {
                    indexbuffer[index] = (col * detail + row);
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;
                    indexbuffer[index] = (col + 1) * detail + row;
                    index++;

                    indexbuffer[index] = (col + 1) * detail + row;
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;
                    indexbuffer[index] = vertexCount + 1;
                    index++;
                }

                vertexCount++;
            }
            for (int row = 0; row < detail; row++)
            {
                int col = 0;
                vertices[vertexCount] = vertices[col * detail + row] + vertices[col * detail + row].normalized * planet.PlanetMeshDefinitionInfo.Radius * borderDepth;
                normals[vertexCount] = normals[col * detail + row];
                uvs[vertexCount] = uvs[col * detail + row];
                vertexColors[vertexCount] = vertexColors[col * detail + row];

                // calculate triangle indexes
                if (row < detail - 1)
                {
                    indexbuffer[index] = (col * detail + row);
                    index++;
                    indexbuffer[index] = col * detail + row + 1;
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;

                    indexbuffer[index] = vertexCount;
                    index++;
                    indexbuffer[index] = col * detail + row + 1;
                    index++;
                    indexbuffer[index] = vertexCount + 1;
                    index++;
                }

                vertexCount++;
            }

            for (int col = 0; col < detail; col++)
            {
                int row = detail - 1;
                vertices[vertexCount] = vertices[col * detail + row] + vertices[col * detail + row].normalized * planet.PlanetMeshDefinitionInfo.Radius * borderDepth;
                normals[vertexCount] = normals[col * detail + row];
                uvs[vertexCount] = uvs[col * detail + row];
                vertexColors[vertexCount] = vertexColors[col * detail + row];

                // calculate triangle indexes
                if (col < detail - 1)
                {
                    indexbuffer[index] = (col * detail + row);
                    index++;
                    indexbuffer[index] = (col + 1) * detail + row;
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;

                    indexbuffer[index] = (col + 1) * detail + row;
                    index++;
                    indexbuffer[index] = vertexCount + 1;
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;
                }

                vertexCount++;
            }
            for (int row = 0; row < detail; row++)
            {
                int col = detail - 1;
                vertices[vertexCount] = vertices[col * detail + row] + vertices[col * detail + row].normalized * planet.PlanetMeshDefinitionInfo.Radius * borderDepth;
                normals[vertexCount] = normals[col * detail + row];
                uvs[vertexCount] = uvs[col * detail + row];
                vertexColors[vertexCount] = vertexColors[col * detail + row];

                // calculate triangle indexes
                if (row < detail - 1)
                {
                    indexbuffer[index] = (col * detail + row);
                    index++;
                    indexbuffer[index] = vertexCount;
                    index++;
                    indexbuffer[index] = col * detail + row + 1;
                    index++;

                    indexbuffer[index] = vertexCount;
                    index++;
                    indexbuffer[index] = vertexCount + 1;
                    index++;
                    indexbuffer[index] = col * detail + row + 1;
                    index++;
                }

                vertexCount++;
            }

            Debug.Log("Mesh calculated in " + (float)(System.DateTime.UtcNow - startTime).TotalSeconds * 1000f + "ms");
		
        }


        #endregion
        #region Destroy And Cleanup
        /// <summary>
        /// Destroys the generated mesh to free VBO's and then destroys the gameobject
        /// </summary>
        public void CleanAndDestroy()
        {
            if (this != null)
            {
                if (this.mesh != null)
                    DestroyImmediate(this.mesh, false);
                if (this.gameObject != null)
                    DestroyImmediate(this.gameObject);
            }
        }

        #endregion
    }

}
