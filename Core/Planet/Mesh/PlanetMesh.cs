﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Events;
using AmadeusRTG.Core.Events.Args;
using AmadeusRTG.Core.Planet.Defenitions;
using AmadeusRTG.Core.Utils.Enums;
using UnityEngine;

namespace AmadeusRTG.Core.Planet.Mesh
{
    public class PlanetMesh : MonoBehaviour
    {
        #region Params

        private List<SurfaceMesh> subdivisionQueue, generationQueue;
        private SurfacePriorityComparer surfaceComparer;

        // how many surfaces can be subdivided at the same time
        public int simultaneousSubdivisions = 1;

        // renderer culling by angle
        public bool useAngleCulling = true;
        public float rendererCullingAngle = 70f;

        // how often lod is updated in seconds
        public float lodUpdateInterval = 0f;
        private float lastUpdateTime = 0f;
        protected float[] topModifier, bottomModifier, leftModifier, rightModifier, frontModifier, backModifier;
        protected int modifierResolution = 128;
        public bool useBicubicInterpolation = true;
        protected bool modifiersInitialized = false;
        public PlanetDefenition PlanetDefenitionInfo;
        public PlanetMeshDefenition PlanetMeshDefinitionInfo;
        protected Action OnIteration;
        private GameObject MeshHolder;
        private int current_level;

        public double NoiseFactor { get; set; }
        // list of terrain surfaces
        public List<SurfaceMesh> surfaces = new List<SurfaceMesh>();

        //lod data
        private bool _useLod = false;
        public float[] lodDistances = new float[1];
        public bool[] generateColliders = new bool[2];
        public bool[] disableCollidersOnSubdivision = new bool[2];
        private int _maxLodLevel = 1;

        public bool UseLod
        {
            get { return _useLod; }
            set
            {
                _useLod = value;
                if (lodDistances == null)
                    lodDistances = new float[_maxLodLevel];
                if (generateColliders == null)
                    generateColliders = new bool[_maxLodLevel + 1];
                if (disableCollidersOnSubdivision == null)
                    disableCollidersOnSubdivision = new bool[_maxLodLevel + 1];
            }
        }

        public int MaxLodLevel
        {
            get { return _maxLodLevel; }
            set
            {
                if (value != _maxLodLevel)
                {
                    _maxLodLevel = value;

                    float[] newLodDistances = new float[_maxLodLevel];
                    if (lodDistances != null)
                    {
                        if (lodDistances.Length != 0)
                        {
                            for (int i = 0; i < newLodDistances.Length; i++)
                            {
                                if (i < lodDistances.Length)
                                {
                                    newLodDistances[i] = lodDistances[i];
                                }
                                else
                                {
                                    newLodDistances[i] = lodDistances[lodDistances.Length - 1]*
                                                         Mathf.Pow(.5f, i - lodDistances.Length + 1);
                                }
                            }
                        }
                    }
                    lodDistances = newLodDistances;

                    bool[] newGenerateColliders = new bool[_maxLodLevel + 1];
                    if (generateColliders != null)
                    {
                        if (generateColliders.Length != 0)
                        {
                            for (int i = 0; i < newLodDistances.Length; i++)
                            {
                                if (i < generateColliders.Length)
                                {
                                    newGenerateColliders[i] = generateColliders[i];
                                }
                                else
                                {
                                    newGenerateColliders[i] = false;
                                }
                            }
                        }
                    }
                    generateColliders = newGenerateColliders;

                    bool[] newDisableCollidersOnSubdivision = new bool[_maxLodLevel + 1];
                    if (disableCollidersOnSubdivision != null)
                    {
                        if (disableCollidersOnSubdivision.Length != 0)
                        {
                            for (int i = 0; i < newLodDistances.Length; i++)
                            {
                                if (i < disableCollidersOnSubdivision.Length)
                                {
                                    newDisableCollidersOnSubdivision[i] = disableCollidersOnSubdivision[i];
                                }
                                else
                                {
                                    newDisableCollidersOnSubdivision[i] = false;
                                }
                            }
                        }
                    }
                    disableCollidersOnSubdivision = newDisableCollidersOnSubdivision;
                }
            }
        }

        #endregion

        #region Unity Events

        private void Start()
        {
            subdivisionQueue = new List<SurfaceMesh>();
            generationQueue = new List<SurfaceMesh>();
        }

        private void Update()
        {
        }

        #endregion

        #region Events

        #region OnPlanetGenerated

        public delegate void PlanetGenerated(object sender, PlanetMeshDataArgs e);

        public event PlanetGenerated OnPlanetGenerated;

        public void TriggerPlanetGenerated(object sender)
        {
            transform.parent = MeshHolder.transform;
            PlanetMeshDataArgs args = new PlanetMeshDataArgs(MeshHolder);
            OnPlanetGenerated(sender, args);
        }

        #endregion 
        #region OnPlanetGenerated

        public event SurfaceMesh.SurfaceDelegate OnSurfaceGenerated;

        #endregion

        #endregion
        #region Generate Planet Mesh
        public void Generate(PlanetDefenition PlanetDefenitionInfo, PlanetMeshDefenition PlanetMeshDefinitionInfo,
            double NoiseFactor)
        {
            Generate(null, PlanetDefenitionInfo, PlanetMeshDefinitionInfo, NoiseFactor);
        }

        public void Generate(Action OnIteration, PlanetDefenition PlanetDefenitionInfo,
            PlanetMeshDefenition PlanetMeshDefinitionInfo, double NoiseFactor)
        {
            PlanetDefenitionInfo = PlanetDefenitionInfo;
            PlanetMeshDefinitionInfo = PlanetMeshDefinitionInfo;
            OnIteration = OnIteration;
            this.NoiseFactor = NoiseFactor;
            MeshHolder = new GameObject();
            // initialize modifiers if not existing already
            if (!modifiersInitialized)
            {
                modifiersInitialized = true;
                ClearModifiers();
            }

            // destroy already existing surfaces before creating new ones
            ClearSurfaces();
            current_level = 1;
            // plot vertex positions for all surfaces
            float step = 2f / PlanetMeshDefinitionInfo.SubdivisionsLevel;
            float half = PlanetMeshDefinitionInfo.SubdivisionsLevel / 2f * step;
            if (PlanetMeshDefinitionInfo.SubdivisionsLevel > 0)
            {
                for (int sY = 0; sY < PlanetMeshDefinitionInfo.SubdivisionsLevel; sY++)
                {
                    float y = sY*step;
                    float ny = (sY + 1)*step;

                    for (int sX = 0; sX < PlanetMeshDefinitionInfo.SubdivisionsLevel; sX++)
                    {
                        float x = sX*step;
                        float nx = (sX + 1)*step;

                        for (int i = 0; i < 6; i++)
                        {
                            CreateSurface(x, half, y, nx, ny, (SurfacePosition) i, sX, sY);
                            if (OnIteration != null)
                                OnIteration();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Gets the nearest modifier at position
        /// </summary>
        public float GetModifierAt(int row, int col, SurfacePosition sp)
        {
            int x = row, y = col;

            SurfacePosition correctSP = GetCorrectSurfacePosition(ref x, ref y, sp);
            //if(x < 0 || x > modifierResolution-1 || y < 0 || y > modifierResolution-1)
            //	Debug.Log("row: " + row + " col: " + col + " sp: " + sp + " | " + " x: " + x + " y: " + y + " correct:" + correctSP.ToString());

            if (x < 0)
                x = 0;
            if (x > modifierResolution - 1)
                x = modifierResolution - 1;
            if (y < 0)
                y = 0;
            if (y > modifierResolution - 1)
                y = modifierResolution - 1;
            return GetModifierArray(correctSP)[y * modifierResolution + x];
        }

        /// <summary>
        /// Gets the bilinearly interpolated modifier at given position. Called by the surface to get modifier height for each point.
        /// </summary>
        public float GetBilinearInterpolatedModifierAt(float row, float col, SurfacePosition sp)
        {
            int row1 = Mathf.FloorToInt(row);
            int col1 = Mathf.FloorToInt(col);
            float interX = row - row1;
            float interY = col - col1;

            float a = GetModifierAt(row1, col1, sp);
            float b = GetModifierAt(row1 + 1, col1, sp);
            float c = GetModifierAt(row1, col1 + 1, sp);
            float d = GetModifierAt(row1 + 1, col1 + 1, sp);

            return Mathf.Lerp(Mathf.Lerp(a, b, interX), Mathf.Lerp(c, d, interX), interY);
        }
        /// <summary>
        /// Gets the bicubicly interpolated modifier at given position. Called by the surface to get modifier height for each point.
        /// </summary>
        public float GetBicubicInterpolatedModifierAt(float row, float col, SurfacePosition sp)
        {
            int row1 = Mathf.FloorToInt(row);
            int col1 = Mathf.FloorToInt(col);
            float interX = row - row1;
            float interY = col - col1;

            float[][] array = new float[4][] { new float[4], new float[4], new float[4], new float[4] };

            array[0][0] = GetModifierAt(row1 - 1, col1 - 1, sp);
            array[0][1] = GetModifierAt(row1 - 1, col1, sp);
            array[0][2] = GetModifierAt(row1 - 1, col1 + 1, sp);
            array[0][3] = GetModifierAt(row1 - 1, col1 + 2, sp);

            array[1][0] = GetModifierAt(row1, col1 - 1, sp);
            array[1][1] = GetModifierAt(row1, col1, sp);
            array[1][2] = GetModifierAt(row1, col1 + 1, sp);
            array[1][3] = GetModifierAt(row1, col1 + 2, sp);

            array[2][0] = GetModifierAt(row1 + 1, col1 - 1, sp);
            array[2][1] = GetModifierAt(row1 + 1, col1, sp);
            array[2][2] = GetModifierAt(row1 + 1, col1 + 1, sp);
            array[2][3] = GetModifierAt(row1 + 1, col1 + 2, sp);

            array[3][0] = GetModifierAt(row1 + 2, col1 - 1, sp);
            array[3][1] = GetModifierAt(row1 + 2, col1, sp);
            array[3][2] = GetModifierAt(row1 + 2, col1 + 1, sp);
            array[3][3] = GetModifierAt(row1 + 2, col1 + 2, sp);

            return GetBiCubicValue(array, interX, interY);
        }

        public static float GetCubicValue(float[] p, float x)
        {
            return p[1] + 0.5f * x * (p[2] - p[0] + x * (2.0f * p[0] - 5.0f * p[1] + 4.0f * p[2] - p[3] + x * (3.0f * (p[1] - p[2]) + p[3] - p[0])));
        }
        public float GetBiCubicValue(float[][] p, float x, float y)
        {
            float[] arr = new float[4];
            arr[0] = GetCubicValue(p[0], y);
            arr[1] = GetCubicValue(p[1], y);
            arr[2] = GetCubicValue(p[2], y);
            arr[3] = GetCubicValue(p[3], y);
            return GetCubicValue(arr, x);
        }

        /// <summary>
        /// Returns the correct surface position when out of bounds of the current one
        /// </summary>
        public SurfacePosition GetCorrectSurfacePosition(ref int row, ref int col, SurfacePosition sp)
        {
            SurfacePosition correctSP = sp;

            switch (sp)
            {
                case SurfacePosition.TOP:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.TOP;

                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.LEFT;
                        int oldCol = col;
                        col = Mathf.Abs(row + 1);
                        row = oldCol;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.RIGHT;
                        int oldCol = col;
                        col = row - modifierResolution;
                        row = (modifierResolution - 1) - oldCol;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.BACK;
                        col = Mathf.Abs(col + 1);
                        row = (modifierResolution - 1) - row;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.FRONT;
                        col -= modifierResolution;
                    }
                    break;
                case SurfacePosition.BOTTOM:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.BOTTOM;

                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.LEFT;
                        int oldCol = col;
                        col = modifierResolution + row;
                        row = (modifierResolution - 1) - oldCol;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.RIGHT;
                        int oldCol = col;
                        col = modifierResolution - (int)Mathf.Abs((modifierResolution - 1) - row);
                        row = oldCol;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.FRONT;
                        col = modifierResolution + col;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BACK;
                        col = modifierResolution - (int)Mathf.Abs((modifierResolution - 1) - col);
                        row = (modifierResolution - 1) - row;
                    }
                    break;
                case SurfacePosition.LEFT:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.LEFT;

                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.BACK;
                        row = modifierResolution + row;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.FRONT;
                        row -= modifierResolution;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.TOP;
                        int oldRow = row;
                        row = Mathf.Abs(col + 1);
                        col = oldRow;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BOTTOM;
                        int oldRow = row;
                        row = col - modifierResolution;
                        col = (modifierResolution - 1) - oldRow;
                    }
                    break;
                case SurfacePosition.RIGHT:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.RIGHT;
                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.FRONT;
                        row = modifierResolution + row;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BACK;
                        row -= modifierResolution;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.TOP;
                        int oldRow = row;
                        row = modifierResolution + col;
                        col = (modifierResolution - 1) - oldRow;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BOTTOM;
                        int oldRow = row;
                        row = modifierResolution - (int)Mathf.Abs((modifierResolution - 1) - col);
                        col = oldRow;
                    }
                    break;
                case SurfacePosition.FRONT:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.FRONT;
                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.LEFT;
                        row = modifierResolution + row;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.RIGHT;
                        row -= modifierResolution;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.TOP;
                        col = modifierResolution + col;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BOTTOM;
                        col -= modifierResolution;
                    }
                    break;
                case SurfacePosition.BACK:
                    if (row >= 0 && row < modifierResolution && col >= 0 && col < modifierResolution)
                        correctSP = SurfacePosition.BACK;
                    else if (row < 0)
                    {
                        correctSP = SurfacePosition.RIGHT;
                        row = modifierResolution + row;
                    }
                    else if (row >= modifierResolution)
                    {
                        correctSP = SurfacePosition.LEFT;
                        row -= modifierResolution;
                    }
                    else if (col < 0)
                    {
                        correctSP = SurfacePosition.TOP;
                        col = Mathf.Abs(col + 1);
                        row = (modifierResolution - 1) - row;
                    }
                    else if (col >= modifierResolution)
                    {
                        correctSP = SurfacePosition.BOTTOM;
                        col = modifierResolution - (int)Mathf.Abs((modifierResolution - 1) - col);
                        row = modifierResolution - row;
                    }
                    break;
            }

            return correctSP;
        }

        /// <summary>
        /// Single iteration generates one surface. To be used to show progress on the editor script
        /// </summary>
        protected void CreateSurface(float x, float half, float y, float nx, float ny, SurfacePosition pos, int sx,
            int sy)
        {
            Vector3 start = Vector3.zero, end = Vector3.zero;
            Vector3 topRight = Vector3.zero, bottomLeft = Vector3.zero;

            switch (pos)
            {
                case SurfacePosition.TOP:
                    start = new Vector3(x - half, half, y - half);
                    end = new Vector3(nx - half, half, ny - half);

                    topRight = new Vector3(nx - half, half, y - half);
                    bottomLeft = new Vector3(x - half, half, ny - half);
                    break;
                case SurfacePosition.FRONT:
                    start = new Vector3(x - half, half - y, half);
                    end = new Vector3(nx - half, half - ny, half);

                    topRight = new Vector3(nx - half, half - y, half);
                    bottomLeft = new Vector3(x - half, half - ny, half);
                    break;
                case SurfacePosition.BOTTOM:
                    start = new Vector3(x - half, -half, half - y);
                    end = new Vector3(nx - half, -half, half - ny);

                    topRight = new Vector3(nx - half, -half, half - y);
                    bottomLeft = new Vector3(x - half, -half, half - ny);
                    break;
                case SurfacePosition.BACK:
                    start = new Vector3(half - x, half - y, -half);
                    end = new Vector3(half - nx, half - ny, -half);

                    topRight = new Vector3(half - nx, half - y, -half);
                    bottomLeft = new Vector3(half - x, half - ny, -half);
                    break;
                case SurfacePosition.LEFT:
                    start = new Vector3(-half, half - y, -half + x);
                    end = new Vector3(-half, half - ny, -half + nx);

                    topRight = new Vector3(-half, half - y, -half + nx);
                    bottomLeft = new Vector3(-half, half - ny, -half + x);
                    break;
                case SurfacePosition.RIGHT:
                    start = new Vector3(half, half - y, half - x);
                    end = new Vector3(half, half - ny, half - nx);

                    topRight = new Vector3(half, half - y, half - nx);
                    bottomLeft = new Vector3(half, half - ny, half - x);
                    break;
            }

            InstantiateSurface(current_level, start, end, topRight, bottomLeft, pos, sx, sy);
            current_level++;
        }

        /// <summary>
        /// Instantiates a surface and adds it to the list. Virtual so other planet types can override it to use different surfaces
        /// </summary>
        private void InstantiateSurface(int id, Vector3 start, Vector3 end, Vector3 topRight, Vector3 bottomLeft,
            SurfacePosition sp, int sx, int sy)
        {
            GameObject t = new GameObject("Surface" + id);
            t.transform.parent = this.transform;
            t.transform.position = transform.position;
            SurfaceMesh surface = t.AddComponent<SurfaceMesh>();
            surface.Initialize(0, start, end, topRight, bottomLeft, this, sp, null, sx, sy);
            surface.GenerateMesh(PlanetMeshDefinitionInfo.Resolution);
            surfaces.Add(surface);
        }

        protected void ClearModifiers()
        {

            // create modifier arrays
            topModifier = new float[modifierResolution*modifierResolution];
            bottomModifier = new float[modifierResolution*modifierResolution];
            leftModifier = new float[modifierResolution*modifierResolution];
            rightModifier = new float[modifierResolution*modifierResolution];
            frontModifier = new float[modifierResolution*modifierResolution];
            backModifier = new float[modifierResolution*modifierResolution];
            //we generete the modifier matix
            for (int x = 0; x < modifierResolution; x++)
            {
                for (int y = 0; y < modifierResolution; y++)
                {
                    topModifier[x*modifierResolution + y] = 0f;
                    bottomModifier[x*modifierResolution + y] = 0f;
                    leftModifier[x*modifierResolution + y] = 0f;
                    rightModifier[x*modifierResolution + y] = 0f;
                    frontModifier[x*modifierResolution + y] = 0f;
                    backModifier[x*modifierResolution + y] = 0f;
                }
            }
        }

        /// <summary>
        /// from the position of the surface we send the current modifier
        /// </summary>
        /// <param name="surfacePosition"></param>
        /// <returns></returns>
        protected float[] GetModifierArray(SurfacePosition surfacePosition)
        {
            switch (surfacePosition)
            {
                case SurfacePosition.TOP:
                    return topModifier;
                case SurfacePosition.BOTTOM:
                    return bottomModifier;
                case SurfacePosition.LEFT:
                    return leftModifier;
                case SurfacePosition.RIGHT:
                    return rightModifier;
                case SurfacePosition.FRONT:
                    return frontModifier;
                case SurfacePosition.BACK:
                    return backModifier;
            }
            return null;
        }

        /// <summary>
        /// Destroys all currently existing surfaces 
        /// </summary>
        public void ClearSurfaces()
        {
            Component[] childComponents = GetComponentsInChildren(typeof (SurfaceMesh)) as Component[];
            if (childComponents.Length > 0)
            {
                foreach (Component c in childComponents)
                {
                    SurfaceMesh s = (SurfaceMesh) c;
                    if (s != null)
                    {
                        // if clearing in editor, dont destroy meshes so undo is possible
                        if (Application.isEditor && !Application.isPlaying)
                            DestroyImmediate(s.gameObject);
                        else
                            s.CleanAndDestroy();
                    }
                }
            }
            surfaces.Clear();
        }
        #endregion

        public void ReportGeneration(SurfaceMesh mesh)
        {
            if (OnSurfaceGenerated !=  null)
            {
                OnSurfaceGenerated(mesh);
            }
        }
    }
}
