﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Exceptions;
using AmadeusRTG.Core.Presets;

namespace AmadeusRTG.Core.Planet.Defenitions
{
    public class PlanetMeshDefenition
    {
        public int Seed { get; private set; }
        public float Radius { get; private set; }
        public PresetLoader PresetNoiseer { get; private set; }
        public float HeightVariation { get; private set; }
        public float FrequencyScale { get; private set; }
        public int SubdivisionsLevel { get; private set; }
        public List<bool> GenerateColliders { get; private set; }
        public int Resolution { get; private set; }

        public PlanetMeshDefenition(int Seed, float Radius, PresetLoader PresetNoiseer, float HeightVariation, float FrequencyScale, int SubdivisionsLevel, List<bool> GenerateColliders, int Resolution = 64)
        {
            if (PresetNoiseer == null)
                throw new PresetNotExistException("Unable load planet Object preset nosie factor is null!");
            this.Seed = Seed;
            this.Radius = Radius;
            this.PresetNoiseer = PresetNoiseer;
            this.HeightVariation = HeightVariation;
            this.FrequencyScale = FrequencyScale;
            this.SubdivisionsLevel = SubdivisionsLevel;
            this.Resolution = Resolution;
            this.GenerateColliders = GenerateColliders;
        }
    }

}
