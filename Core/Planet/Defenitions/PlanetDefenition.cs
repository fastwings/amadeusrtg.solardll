﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Utils.Enums;
using UnityEngine;

namespace AmadeusRTG.Core.Planet.Defenitions
{
    public class PlanetDefenition
    {
        public Vector3 StartPostion { get; private set; }
        public PlanetOrbitDirection PlanetOrbitDirection { get; private set; }
        public float OrbitAngel { get; private set; }
        public float OrbitSpeed { get; private set; }
        public bool HasAtmosphere { get; private set; }
        public bool HasWater { get; private set; }
        public bool HasRings { get; private set; }
        public int TotalRings { get; private set; }
        public Vector3 RoutionPostion { get; private set; }
        public float RoutionSpeed { get; private set; }
        public List<GameObject> DefferDecalObjects { get; private set; }

        public PlanetDefenition(Vector3 StartPostion, PlanetOrbitDirection PlanetOrbitDirection, float OrbitAngel, float OrbitSpeed, bool HasAtmosphere,
            bool HasWater, bool HasRings, int TotalRings, Vector3 RoutionPostion, float RoutionSpeed)
        {
            this.StartPostion = StartPostion;
            this.OrbitAngel = OrbitAngel;
            this.PlanetOrbitDirection = PlanetOrbitDirection;
            this.OrbitSpeed = OrbitSpeed;
            this.HasAtmosphere = HasAtmosphere;
            this.HasRings = HasRings;
            this.HasWater = HasWater;
            this.TotalRings = TotalRings;
            this.RoutionPostion = RoutionPostion;
            this.RoutionSpeed = RoutionSpeed;
        }
    }
}
