﻿using System.Configuration;
using System.Linq;
using AmadeusRTG.Core.Events;
using AmadeusRTG.Core.Exceptions;
using AmadeusRTG.Core.Utils;
using AmadeusRTG.Core.Utils.Enums;
using AmadeusRTG.Core.Utils.Enums.Config;
using AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition;
using AmadeusRTG.Core.Utils.Generics;
using UnityEngine;
using Random = System.Random;

namespace AmadeusRTG.Core.API
{
    public class PlanetAllocator
    {
        public static PlanetAllocator CreatePlanetAllocator()
        {
            return new PlanetAllocator();
        }
        public static PlanetAllocator CreatePlanetAllocator(string definition_file)
        {
            return new PlanetAllocator(definition_file);
        }

        private ConfigLoader config_object;
        private string config_file;
        public int Seed { get; set; }
        public PlanetAllocator()
        {
            InitConfigParams();
            WireAllocaterEvents();
            EventManager.Instance.LoadConfigInvoker(this, config_file, ConfigLoaderActionType.Load);
        }

        public PlanetAllocator(string definition_file)
        {
            if (definition_file == "")
                throw new PresetNotExistException("definition file Cant be empty!");
            Debug.Log(string.Format("Planet Allocator Init"));
            Debug.Log(string.Format("Planet Allocator Definition: {0}",definition_file));
            InitConfigParams();
            WireAllocaterEvents();
            EventManager.Instance.LoadConfigInvoker(this, string.Format("{0}.cfg", config_file), ConfigLoaderActionType.Load);
        }

        void InitConfigParams()
        {
            config_file = Enum<ConfigFiles>.ToString(ConfigFiles.PlanetDefinition);
        }

        void WireAllocaterEvents()
        {
            EventManager.Instance.OnConfigLoad += Instance_OnConfigLoad;
            EventManager.Instance.OnPresetLoad += Instance_OnPresetLoad;
        }

        #region Events
        void Instance_OnPresetLoad(object sender, Events.Args.LoadPresetEventArgs e)
        {
            Debug.Log(string.Format("Perset Loaded: {0}", e.FileName));
            e.PresetObject.Parse();
            var output = e.PresetObject.GetValue(new Vector3(-41, 41, 0));
            Debug.Log(string.Format("Perset Parsed: {0}", output));
            EventManager.Instance.PlanetAllocator.LoadPlanetAllocateDoneInvoker(this, e.PresetObject, output);
        }

        void Instance_OnConfigLoad(object sender, Events.Args.LoadConfigEventArgs e)
        {
            if (sender.GetType() == typeof (PlanetAllocator))
            {
                Debug.Log(string.Format("Config Loaded: {0}", e.FileName));
                config_object = e.ConfigObject;
                string config_isRand_key = Enum<ConfigAllocateKeys>.ToString(ConfigAllocateKeys.IsRnd);
                string config_seed_key = Enum<ConfigAllocateKeys>.ToString(ConfigAllocateKeys.Seed);
                bool config_isRand_value =
                    config_object.GetValue(
                        Enum<AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections>.ToString(
                            AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections.Allocte), config_isRand_key, false);
                if (config_isRand_value)
                {
                    Seed = -1;
                }
                else
                {
                    Seed =
                        config_object.GetValue(
                            Enum<AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections>.ToString(
                                AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections.Allocte), config_seed_key, -1);
                }
                EventManager.Instance.PlanetAllocator.LoadPlanetAllocateReadyInvoker(this);
            }
        }
        #endregion

        public void AllocatePlanet()
        {
            Random rand =new Random();
            string config_persets_key = Enum<ConfigPresetsKeys>.ToString(ConfigPresetsKeys.ExistedPresets);
            string config_persets_value = config_object.GetValue(Enum<AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections>.ToString(AmadeusRTG.Core.Utils.Enums.Config.PlanetDefinition.ConfigSections.Presets), config_persets_key, "");
            if (config_persets_value == "")
                throw new PresetNotPresentException("No Runtime Presets on config definition system cant run");
            var persets = config_persets_value.Split(',');
            string selected_perset = persets[rand.Next(0, persets.Length)]; // we select random preset for the system
            EventManager.Instance.LoadPresetInvoker(this, string.Format("{0}.json", selected_perset));
        }

       
    }
}
