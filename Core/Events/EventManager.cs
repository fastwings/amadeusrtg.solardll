﻿using AmadeusRTG.Core.Events.Args;
using AmadeusRTG.Core.Utils.Enums;

namespace AmadeusRTG.Core.Events
{
    public class EventManager
    {
        private EventManager() { }
        private static EventManager instrance;
        public static EventManager Instance
        {
            get
            {
                if (instrance == null) 
                    instrance = new EventManager();
                return instrance;
            }
        }

        public PlanetAllocatorEvents PlanetAllocator
        {
            get { return PlanetAllocatorEvents.Instance; }
        }

        #region Events
        #region OnConfigLoad
        public delegate void LoadConfigObject(object sender, LoadConfigEventArgs e);
        public event LoadConfigObject OnConfigLoad;
        public void LoadConfigInvoker(object sender, string filename, ConfigLoaderActionType eventType)
        {
            LoadConfigEventArgs args = new LoadConfigEventArgs(filename, eventType);
            OnConfigLoad(sender, args);
        }
        #endregion

        #region OnPresetLoad
        public delegate void LoadPresetObject(object sender, LoadPresetEventArgs e);
        public event LoadPresetObject OnPresetLoad;
        public void LoadPresetInvoker(object sender,string filename) 
        {
            LoadPresetEventArgs args = new LoadPresetEventArgs(filename);
            OnPresetLoad(sender, args);
        }
        #endregion
        #endregion
    }
}
