﻿using System;
using AmadeusRTG.Core.Presets;

namespace AmadeusRTG.Core.Events.Args
{
    public class LoadPlanetAllocateEventArgs : EventArgs
    {

        public double Output { get; private set; }
        public PresetLoader PresetObject { get; private set; }

        public LoadPlanetAllocateEventArgs(PresetLoader PresetObject,double Output)
        {
            this.Output = Output;
            this.PresetObject = PresetObject;
        }
    }
}
