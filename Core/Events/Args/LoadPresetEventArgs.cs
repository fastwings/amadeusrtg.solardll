﻿using System;
using System.IO;
using AmadeusRTG.Core.Presets;
using AmadeusRTG.Core.Source.Utils;

namespace AmadeusRTG.Core.Events.Args
{
    public class LoadPresetEventArgs : EventArgs
    {
        public string FileName { get; private set; }
        public PresetLoader PresetObject { get; private set; }

        public LoadPresetEventArgs(string fileName)
        {
            FileName = fileName;
            PresetObject = new PresetLoader(Path.Combine(Directorys.GetGamePresetDirectory(), fileName));
        }
    }
}
