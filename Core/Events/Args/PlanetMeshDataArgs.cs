﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace AmadeusRTG.Core.Events.Args
{
    public class PlanetMeshDataArgs : EventArgs
    {
        public GameObject PlanetMeshData { get; private set; }

        public PlanetMeshDataArgs(GameObject PlanetMeshData)
        {
            this.PlanetMeshData = PlanetMeshData;
        }
    }
}
