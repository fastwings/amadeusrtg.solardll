﻿using System;
using System.IO;
using AmadeusRTG.Core.Source.Utils;
using AmadeusRTG.Core.Utils;
using AmadeusRTG.Core.Utils.Enums;

namespace AmadeusRTG.Core.Events.Args
{
    public class LoadConfigEventArgs : EventArgs
    {
        public ConfigLoaderActionType EventType { get; private set; }
        public string FileName { get; private set; }
        public ConfigLoader ConfigObject { get; private set; }

        public LoadConfigEventArgs(string fileName, ConfigLoaderActionType eventType)
        {
            FileName = fileName;
            EventType = eventType;
            ConfigObject = new ConfigLoader(Path.Combine(Directorys.GetGameConfigDirectory(), fileName));
        }
    }
}
