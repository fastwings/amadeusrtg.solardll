﻿using System;
using AmadeusRTG.Core.Events.Args;
using AmadeusRTG.Core.Presets;

namespace AmadeusRTG.Core.Events
{
    public class PlanetAllocatorEvents
    {
        private PlanetAllocatorEvents() { }
        private static PlanetAllocatorEvents instrance;
        public static PlanetAllocatorEvents Instance
        {
            get
            {
                if (instrance == null)
                    instrance = new PlanetAllocatorEvents();
                return instrance;
            }
        }
        #region OnPlanetAllocatReady
        public delegate void LoadPlanetAllocateReady(object sender, EventArgs e);
        public event LoadPlanetAllocateReady OnPlanetAllocateReady;
        public void LoadPlanetAllocateReadyInvoker(object sender)
        {
            EventArgs args = new EventArgs();
            OnPlanetAllocateReady(sender, args);
        }
        #endregion
        #region OnPlanetAllocateDone
        public delegate void LoadPlanetAllocateDone(object sender, LoadPlanetAllocateEventArgs e);
        public event LoadPlanetAllocateDone OnPlanetAllocateDone;
        public void LoadPlanetAllocateDoneInvoker(object sender, PresetLoader PresetObject, double Output)
        {
            LoadPlanetAllocateEventArgs args = new LoadPlanetAllocateEventArgs(PresetObject, Output);
            OnPlanetAllocateDone(sender, args);
        }
        #endregion
    }
}
