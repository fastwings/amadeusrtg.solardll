﻿using AmadeusRTG.Core.Utils.Enums;

namespace AmadeusRTG.Core.Presets.Models
{
    public class PresetCompileOprationModel
    {
        public float Output { get; set; }
        public PresetOprationModel Model { get; private set; }
        public PesetOprationModelsSection SectionModel { get; private set; }
        public PesetOprationGeneratorTypeModels GeneratorTypeModel { get; private set; }
        public PesetOprationOperatorTypeModels OperatorTypeModel { get; private set; }
        public PresetOprationDefenintionModel DefenintionModel { get; set; }

        public PresetCompileOprationModel(PresetOprationModel Model, PesetOprationModelsSection Section, PesetOprationGeneratorTypeModels opration)
        {
            this.Model = Model;
            this.SectionModel = Section;
            this.GeneratorTypeModel = opration;
            DefenintionModel = BuildDefentintion(Model);
            Output = 0f;
        }
        public PresetCompileOprationModel(PresetOprationModel Model, PesetOprationModelsSection Section, PesetOprationOperatorTypeModels opration)
        {
            this.Model = Model;
            this.SectionModel = Section;
            this.OperatorTypeModel = opration;
            Output = 0f;
        }

        public static PresetOprationDefenintionModel BuildDefentintion(PresetOprationModel presetOprationModel)
        {
            PresetOprationDefenintionModel model = null;
            if (presetOprationModel.Section == PesetOprationModelsSection.Generator)
            {
                PresetOprationFieldsGeneratorModel fieldModel = new PresetOprationFieldsGeneratorModel();
                fieldModel.Frequency = presetOprationModel.Fields["Frequency"];
                fieldModel.IsRandSeed = (PresetLoader.Seed == -1) ? true : false;
                fieldModel.Seed = PresetLoader.Seed;
                fieldModel.Persistence = presetOprationModel.Fields["Persistence"];
                fieldModel.Octave = (int)presetOprationModel.Fields["Octave"];
                fieldModel.Distance = ((int)presetOprationModel.Fields["Distance"] == 1) ? true : false;
                fieldModel.Displacement = presetOprationModel.Fields["Displacement"];
                fieldModel.Lacunarity = presetOprationModel.Fields["Lacunarity"];
                fieldModel.Value = presetOprationModel.Fields["Value"];
                model = new PresetOprationDefenintionModel(presetOprationModel.Section, presetOprationModel.Type, fieldModel);
            }
            return model;
        }
    }
}
