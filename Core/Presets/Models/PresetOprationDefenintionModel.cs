﻿using AmadeusRTG.Core.Utils.Enums;

namespace AmadeusRTG.Core.Presets.Models
{
    public class PresetOprationDefenintionModel
    {
          /// <summary>
        /// Type of Section (0 - Generator , 1 - Operator) 
        /// </summary>
        public PesetOprationModelsSection Section { get;  set; }
        public string Type { get;  set; }

        public PresetOprationFieldsGeneratorModel PresetOprationFieldGeneratorModel { get; set; }
        public PresetOprationFieldsOperatorModel PresetOprationFieldOperatorModel { get; set; }

        public PresetOprationDefenintionModel(PesetOprationModelsSection Section, string Type,
            PresetOprationFieldsGeneratorModel PresetOprationFieldGeneratorModel)
        {
            this.Section = Section;
            this.Type = Type;
            this.PresetOprationFieldGeneratorModel = PresetOprationFieldGeneratorModel;
        }

        public PresetOprationDefenintionModel(PesetOprationModelsSection Section, string Type,
            PresetOprationFieldsOperatorModel PresetOprationFieldOperatorModel)
        {
            this.Section = Section;
            this.Type = Type;
            this.PresetOprationFieldOperatorModel = PresetOprationFieldOperatorModel;
        }

    }
}
