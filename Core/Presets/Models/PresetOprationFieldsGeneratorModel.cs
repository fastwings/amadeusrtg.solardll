﻿using System;

namespace AmadeusRTG.Core.Presets.Models
{
    public class PresetOprationFieldsGeneratorModel
    {
        private int seed;
        public int Seed
        {
            get
            {
                if (IsRandSeed)
                {
                    Random rand = new Random();
                    seed = rand.Next(99999, 999999);
                }
                return seed;
            }
            set
            {
                if (!IsRandSeed)
                {
                    seed = value;
                }
            }
        }

        public bool IsRandSeed { get; set; }
        public double Frequency { get; set; }
        public double Lacunarity { get; set; }
        public int Octave { get; set; }
        public double Persistence { get; set; }
        public double Displacement { get; set; }
        public bool Distance { get; set; }
        public double Value { get; set; }

        public PresetOprationFieldsGeneratorModel(int seed)
        {
            IsRandSeed = false;
            Seed = seed;
        }

        public PresetOprationFieldsGeneratorModel()
        {
            IsRandSeed = true;
        }
    }
}
