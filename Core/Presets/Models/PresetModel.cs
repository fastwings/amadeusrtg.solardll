﻿namespace AmadeusRTG.Core.Presets.Models
{
    public class PresetModel
    {
        /// <summary>
        /// name of the preset
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Oprations under this preset
        /// </summary>
        public PresetOprationModel[] Oprations { get; set; }
    }

}
