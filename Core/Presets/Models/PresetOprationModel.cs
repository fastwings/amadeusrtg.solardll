﻿using System;
using System.Collections.Generic;
using AmadeusRTG.Core.Utils.Enums;

namespace AmadeusRTG.Core.Presets.Models
{
    public class PresetOprationModel
    {
        /// <summary>
        /// Type of Section (0 - Generator , 1 - Operator) 
        /// </summary>
        public PesetOprationModelsSection Section { get; set; }
        public string Type { get; set; }
        public Dictionary<string, double> Fields { get; set; }


    }
}
