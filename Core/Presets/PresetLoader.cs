﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AmadeusRTG.Core.Json;
using AmadeusRTG.Core.Presets.Models;
using AmadeusRTG.Core.Utils.Enums;
using AmadeusRTG.Core.Utils.Generics;
using UnityEngine;
using AmadeusRTG.Core.Exceptions;
using AmadeusRTG.Core.Source.Utils;
using Random = UnityEngine.Random;

namespace AmadeusRTG.Core.Presets
{
    public class PresetLoader
    {
        public static bool IsRandPreset = true;
        public static PresetModel SelectPresetModel = null;
        public static int Seed { get; set; }
        private List<string> PersetsList { get; set; }

        /// <summary>
        /// List of preset before checking if valid data
        /// </summary>
        private List<PresetModel> PresetModelsRaw { get; set; }
        /// <summary>
        /// list of preset that run and valid
        /// </summary>
        public Dictionary<PresetModel, List<PresetCompileOprationModel>> PresetModels { get; set; }
        public List<PresetModel> PresetModelsFaild { get; set; }
        public PresetLoader()
        {
            PresetModelsRaw = new List<PresetModel>();
            PresetModels = new Dictionary<PresetModel, List<PresetCompileOprationModel>>();
            PresetModelsFaild = new List<PresetModel>();
            PersetsList = new List<string>();
            PersetsList = ScanPresets();
        }

        public PresetLoader(string presetName)
        {
            if (IsPresetExisted(presetName))
            {
                PresetModelsRaw = new List<PresetModel>();
                PresetModels = new Dictionary<PresetModel, List<PresetCompileOprationModel>>();
                PresetModelsFaild = new List<PresetModel>();
                PersetsList = new List<string>();
                PersetsList.Add(presetName);
            }
            else
            {
                throw new PresetNotExistException(string.Format("No Preset {0} Found",presetName));
            }
        }

        public void Parse()
        {
            foreach (var preset in PersetsList)
            {
                PresetModel model = JsonMapper.ToObject<PresetModel>(GetContentFile(preset));
                PresetModelsRaw.Add(model);
            }

            foreach (var preset in PresetModelsRaw)
            {
                if (preset.Oprations.Length == 0)
                {
                    Debug.Log("No preset found");
                }
                else
                {
                    List<PresetCompileOprationModel> models = new List<PresetCompileOprationModel>();
                    bool isValidOprations = false;
                    foreach (var opration in preset.Oprations)
                    {
                        if (IsPresetValid(opration))
                        {
                            if (opration.Section == PesetOprationModelsSection.Generator)
                            {
                                PresetCompileOprationModel model = new PresetCompileOprationModel(opration,
                                    PesetOprationModelsSection.Generator,
                                    Enum<PesetOprationGeneratorTypeModels>.Parse(opration.Type));
                                models.Add(model);
                            }
                            if (opration.Section == PesetOprationModelsSection.Operator)
                            {
                                PresetCompileOprationModel model = new PresetCompileOprationModel(opration,
                                    PesetOprationModelsSection.Operator,
                                    Enum<PesetOprationOperatorTypeModels>.Parse(opration.Type));
                                models.Add(model);
                            }
                            isValidOprations = true;
                        }
                    }

                    //we check if preset ok else we kill the add feed
                    if (isValidOprations)
                    {
                        PresetModels.Add(preset, models);
                    }
                    else
                        PresetModelsFaild.Add(preset);
                }
            }
        }

        public double GetValue(Vector3 postion)
        {
            double output;
            if (IsRandPreset || SelectPresetModel == null)
            {
                if (PresetModels.Count == 0)
                    throw new PresetNotPresentException();
                int selectIdx = Random.Range(0, PresetModels.Count - 1);
                List<PresetModel> keyList = new List<PresetModel>(PresetModels.Keys);
                PresetCompiler compiler = new PresetCompiler(keyList[selectIdx], PresetModels[keyList[selectIdx]], postion);
                output = compiler.Parse();
            }
            else
            {
                var selectModel = PresetModels[SelectPresetModel];
                PresetCompiler compiler = new PresetCompiler(SelectPresetModel, selectModel, postion);
                output = compiler.Parse();    
            }

            return output;
        }
        #region Preset Validtion 

        public bool IsPresetValid(PresetOprationModel model)
        {
            bool isValid = false;
            if (IsPresetSectionValid(model.Section))
            {
                switch (model.Section)
                {
                    case PesetOprationModelsSection.Generator:
                        isValid = IsPresetDataGeneratorValid(model);
                        break;
                    case PesetOprationModelsSection.Operator:
                        isValid = IsPresetDataOperatorValid(model);
                        break;
                }
            }
            return isValid; 
        }

        private bool IsPresetDataGeneratorValid(PresetOprationModel model)
        {
            bool isValid = false;
            foreach (PesetOprationGeneratorTypeModels p in Enum.GetValues(typeof(PesetOprationGeneratorTypeModels)))
             {
                 string enum_name = Enum<PesetOprationGeneratorTypeModels>.ToString(p).ToLower();
                 string type_name = model.Type.ToLower();
                 if (enum_name.Equals(type_name))
                 {
                     isValid = true;
                     break;
                 }
             }
             return isValid;
        }

        private bool IsPresetDataOperatorValid(PresetOprationModel model)
        {
            bool isValid = false;
            foreach (PesetOprationOperatorTypeModels p in Enum.GetValues(typeof(PesetOprationOperatorTypeModels)))
            {
                string enum_name = Enum<PesetOprationOperatorTypeModels>.ToString(p);
                if (enum_name.ToLower() == model.Type.ToLower())
                {
                    isValid = true;
                    break;
                }
            }
            return isValid;
        }


        private bool IsPresetSectionValid(PesetOprationModelsSection model)
        {
            return (model == PesetOprationModelsSection.Generator || model == PesetOprationModelsSection.Operator);
        }
        #endregion
        #region IO Defenintoin

        private string GetContentFile(string file)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(file))
            {
                String line;
                // Read and display lines from the file until the end of 
                // the file is reached.
                while ((line = sr.ReadLine()) != null)
                {
                    sb.AppendLine(line);
                }
            }
            return sb.ToString();
        }
        private bool IsPresetExisted(string presetName )
        {
            return File.Exists(presetName);
        }
        private List<string> ScanPresets()
        {
            List<string> response = new List<string>();
            var files = Directory.GetFiles(Directorys.GetGameConfigDirectory(),"*.json");
            int numFiles = files.Length;
            for (int i = 0; i <= numFiles; i++)
            {
                string file = files[i].ToString();
                response.Add(file);
            }
            return response;
        }
        #endregion
    }
}
