﻿using System;
using System.Collections.Generic;
using LibNoise.Unity;
using LibNoise.Unity.Generator;
using AmadeusRTG.Core.Presets.Models;
using AmadeusRTG.Core.Utils.Enums;
using UnityEngine;

namespace AmadeusRTG.Core.Presets
{
    internal abstract class PresetCompilerBuilder
    {
        public PresetModel Model { get; protected set; }
        public List<PresetCompileOprationModel> ComplieOprations { get; protected set; }
        public Vector3 Postion { get; protected set; }
        public abstract double Parse();
        protected double parseGeneratorType(double output, PresetCompileOprationModel opration)
        {
            switch (opration.GeneratorTypeModel)
            {
                case PesetOprationGeneratorTypeModels.Billow:
                    output = ParseGeneratorBillow(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Checker:
                    output = ParseGeneratorChecker(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Const:
                    output = ParseGeneratorConst(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Cylinder:
                    output = ParseGeneratorCylinder(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Perlin:
                    output = ParseGeneratorPerlin(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.RidgedMultifractal:
                    output = ParseGeneratorRidgedMultifractal(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Spheres:
                    output = ParseGeneratorSpheres(output, opration.DefenintionModel);
                    break;
                case PesetOprationGeneratorTypeModels.Voronoi:
                    output = ParseGeneratorVoronoi(output, opration.DefenintionModel);
                    break;

            }
            return output;
        }

        protected void parseOperatorType(double output, PresetCompileOprationModel opration)
        {
            throw new NotImplementedException();
        }
        #region Generator Type Parsing

        protected double ParseGeneratorBillow(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Billow model = new Billow(
                defenintionModel.PresetOprationFieldGeneratorModel.Frequency, 
                defenintionModel.PresetOprationFieldGeneratorModel.Lacunarity,
                defenintionModel.PresetOprationFieldGeneratorModel.Persistence,
                (int)defenintionModel.PresetOprationFieldGeneratorModel.Octave,
                defenintionModel.PresetOprationFieldGeneratorModel.Seed,
                QualityMode.High);
            return (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorChecker(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Checker model = new Checker();
            return (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorCylinder(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Cylinders model = new Cylinders(defenintionModel.PresetOprationFieldGeneratorModel.Frequency);
            return output + (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorConst(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Const model = new Const(defenintionModel.PresetOprationFieldGeneratorModel.Value);
            return output + (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorPerlin(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Perlin model = new Perlin(
                defenintionModel.PresetOprationFieldGeneratorModel.Frequency,
                defenintionModel.PresetOprationFieldGeneratorModel.Lacunarity,
                defenintionModel.PresetOprationFieldGeneratorModel.Persistence,
                (int)defenintionModel.PresetOprationFieldGeneratorModel.Octave,
                PresetLoader.Seed,
                QualityMode.High);
            return output + (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorRidgedMultifractal(double output, PresetOprationDefenintionModel defenintionModel)
        {
            RidgedMultifractal model = new RidgedMultifractal(
                defenintionModel.PresetOprationFieldGeneratorModel.Frequency,
                defenintionModel.PresetOprationFieldGeneratorModel.Lacunarity,
                (int) defenintionModel.PresetOprationFieldGeneratorModel.Octave,
                PresetLoader.Seed,
                QualityMode.High);
            return output + (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorSpheres(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Spheres model = new Spheres(defenintionModel.PresetOprationFieldGeneratorModel.Frequency);
            return output + (double)model.GetValue(Postion);
        }
        protected double ParseGeneratorVoronoi(double output, PresetOprationDefenintionModel defenintionModel)
        {
            Voronoi model = new Voronoi(defenintionModel.PresetOprationFieldGeneratorModel.Frequency,
                defenintionModel.PresetOprationFieldGeneratorModel.Displacement,
                PresetLoader.Seed,
                defenintionModel.PresetOprationFieldGeneratorModel.Distance);
            return output + (double)model.GetValue(Postion);
        }
        #endregion
    }
}
