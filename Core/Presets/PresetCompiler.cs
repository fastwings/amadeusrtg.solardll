﻿using System.Collections.Generic;
using AmadeusRTG.Core.Presets.Models;
using AmadeusRTG.Core.Utils.Enums;
using UnityEngine;

namespace AmadeusRTG.Core.Presets
{
    internal class PresetCompiler : PresetCompilerBuilder
    {
        public PresetCompiler(PresetModel model, List<PresetCompileOprationModel> complieOprations, Vector3 postion)
        {
            Model = model;
            ComplieOprations = complieOprations;
            Postion = postion;
        }

        public override double Parse()
        {
            double output = 0f;
            foreach (PresetCompileOprationModel opration in ComplieOprations)
            {
                if (opration.SectionModel == PesetOprationModelsSection.Generator)
                {
                    output = parseGeneratorType(output,opration);
                }
                if (opration.SectionModel == PesetOprationModelsSection.Operator)
                {
                    parseOperatorType(output, opration);
                }
            }
            return output;
        }


    }
}
