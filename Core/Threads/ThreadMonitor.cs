﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace AmadeusRTG.Core.Threads
{
    public class ThreadMonitor : MonoBehaviour
    {
        private static bool initialized = false;
        private static ThreadMonitor instance;
        public static int MaxNumberOfTheards = 12;
        private List<Action> actions = new List<Action>();
        private List<Action> currentActions = new List<Action>();
        private static int numThreads = 0;

        public static ThreadMonitor Instance
        {
            get
            {
                if (!initialized)
                {
                    if (!Application.isPlaying)
                        return null;

                    initialized = true;

                    GameObject go = new GameObject("ThreadMonitor");
                    instance = go.AddComponent<ThreadMonitor>();
                }
                return instance;
            }
        }

        private void OnDisable()
        {
            if (instance == this)
            {
                instance = null;
            }
        }

// Awake is called when the script instance is being loaded (Since v1.0)
        private void Awake()
        {
            instance = this;
            initialized = true;
        }

        public static void RunOnMainThread(Action action)
        {
            lock (Instance.actions)
            {
                Instance.actions.Add(action);
            }
        }

        public static Thread RunOnThread(Action action)
        {
            Initialize();

            while (numThreads >= MaxNumberOfTheards)
            {
                Thread.Sleep(1);
            }

            Interlocked.Increment(ref numThreads);

            ThreadPool.QueueUserWorkItem(RunAction, action);

            return null;
        }

        static void Initialize()
        {
            if (!initialized)
            {
                if (!Application.isPlaying)
                    return;

                initialized = true;

                GameObject go = new GameObject("ThreadMonitor");
                instance = go.AddComponent<ThreadMonitor>();
            }

        }
        private static void RunAction(object action)
        {
            try
            {
                ((Action)action)();
            }
            catch { }
            finally
            {
                Interlocked.Decrement(ref numThreads);
            }

        }

        void Update()
        {
            lock (actions)
            {
                currentActions.Clear();
                currentActions.AddRange(actions);
                actions.Clear();
            }
            for (int i = 0; i < currentActions.Count; i++)
                currentActions[i]();
        }
    }
}
