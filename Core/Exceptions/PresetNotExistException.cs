﻿using System;

namespace AmadeusRTG.Core.Exceptions
{
    public class PresetNotExistException : Exception
    {
        public PresetNotExistException(string message):base(message)
        {
            
        }
    }
}
