﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmadeusRTG.Core.Exceptions
{
    public class PlanetDefinitionNullException: Exception
    {
        public PlanetDefinitionNullException()
            : base("Planet Definition Object is null Can't create planet")
        { }
    }
}
