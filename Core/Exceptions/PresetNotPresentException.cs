﻿using System;

namespace AmadeusRTG.Core.Exceptions
{
    public class PresetNotPresentException : Exception
    {
        public PresetNotPresentException()
            : base("No Preset found on so there no data output!")
        { }
        public PresetNotPresentException(string message)
            : base(message)
        { }
    }


}
