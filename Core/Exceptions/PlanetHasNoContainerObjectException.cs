﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmadeusRTG.Core.Exceptions
{
    public class PlanetHasNoContainerObjectException : Exception
    {
        public PlanetHasNoContainerObjectException()
            : base("No Container Object is null Can't create planet")
        { }
    }
}
