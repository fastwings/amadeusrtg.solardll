﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AmadeusRTG.Core.Exceptions
{
    public class PlanetMeshDefinitionNullException: Exception
    {
        public PlanetMeshDefinitionNullException()
            : base("Planet Mesh Definition Object is null Can't create planet")
        { }
    }
}
