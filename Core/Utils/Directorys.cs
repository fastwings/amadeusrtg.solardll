﻿using System.IO;
using UnityEngine;

namespace AmadeusRTG.Core.Source.Utils
{
    public static class Directorys
    {
        public static string GetGameConfigDirectory()
        {
            return Path.Combine(Application.dataPath, Path.Combine("Resources", "Config"));
        }
        public static string GetGamePresetDirectory()
        {
            return Path.Combine(Application.dataPath, Path.Combine("Resources", "Presets"));
        }
    }
}
