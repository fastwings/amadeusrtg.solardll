﻿namespace AmadeusRTG.Core.Utils.Enums.Config
{
    public enum ConfigFiles
    {
        PlanetDefinition
    }

    namespace PlanetDefinition
    {
        public enum ConfigSections
        {
            Allocte,
            Presets
        }
        public enum ConfigAllocateKeys
        {
            IsRnd,
            Seed
        }

        public enum ConfigPresetsKeys
        {
            ExistedPresets
        }

    }
}
