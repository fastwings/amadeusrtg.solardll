﻿namespace AmadeusRTG.Core.Utils.Enums
{


    /// <summary>
    /// Surface position enumerator for indicating surface positions around the planet
    /// </summary>
    public enum SurfacePosition
    {
        TOP, FRONT, BOTTOM, BACK, LEFT, RIGHT
    }

    public enum PlanetOrbitDirection
    {
        ClockWise,
        NotClockWise
    }
    /// <summary>
    /// Config loader action type
    /// </summary>
    public enum ConfigLoaderActionType
    {
        Save,
        Load
    }
    /// <summary>
    /// Enum of the preset of the section type of the operation node
    /// </summary>
    public enum PesetOprationModelsSection
    {
        Generator = 0,
        Operator = 1
    }
    /// <summary>
    /// Enum for the preset existing Generator
    /// </summary>
    public enum PesetOprationGeneratorTypeModels
    {
        Billow,
        Checker,
        Const,
        Cylinder,
        Perlin,
        RidgedMultifractal,
        Spheres,
        Voronoi
    }
    /// <summary>
    /// Enum of the preset existing Operator
    /// </summary>
    public enum PesetOprationOperatorTypeModels
    {
        Abs,
        Add,
        Blend,
        Cache,
        Curve,
        Displace,
        Exponent,
        Invert,
        Max,
        Min,
        Multiply,
        Power,
        Rotate,
        Scale,
        ScaleBias,
        Select,
        Subtract,
        Terrace,
        Translate,
        Turbulence
    }
}
